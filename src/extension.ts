// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as cp from "child_process";


// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	// console.log('Congratulations, your extension "ether-remote" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('ether-remote.addConnection', () => {
		// The code you place here will be executed every time your command is executed
		// Display a message box to the user
		var option: vscode.InputBoxOptions = {
			ignoreFocusOut: false,
			placeHolder: "Input Host name or IP...",
			prompt: "Type here host or IP server."
		};
		vscode.window.showInputBox(option).then(async function (res) {
			let config = vscode.workspace.getConfiguration();
			if (res) {
				// console.log("Ip/Host: ", res);
				config.update('server.host', res);
				var option: vscode.InputBoxOptions = {
					ignoreFocusOut: false,
					placeHolder: "Input directory target",
					prompt: "Type here server directory."
				};
				vscode.window.showInputBox(option).then(async function (res) {
					if (res) {
						// console.log("dir: ", res);
						config.update('server.dir', res);
					} else {
						vscode.window.showErrorMessage("Cannot set taget directory");
					}
				});
			} else {
				vscode.window.showErrorMessage("Cannot set host");
			}
		});

	});

	context.subscriptions.push(disposable);

	disposable = vscode.commands.registerCommand('ether-remote.uploadFile', () => {
		let config = vscode.workspace.getConfiguration();
		//read ip / host setting
		let host = config.get('server.host');
		let dir = config.get('server.dir');

		//read directory path
		let curDir = vscode.workspace.workspaceFolders;
		if (curDir) {
			// let sh = `scp -prq ${curDir[0].uri.path}/. ${host}:${dir}`;
			let sh=`rsync -avzu --delete --no-o --no-g ${curDir[0].uri.path}/ ${host}:${dir}`;
			// let sh = "pwds";
			console.log(sh);
			vscode.window.withProgress({
				location: vscode.ProgressLocation.Window,
				cancellable: false,
				title: 'Uploading files to server'
			}, async (progress) => {
				progress.report({ increment: 0 });

				await new Promise<String>((resolve, reject) => {
					var kd = (err: cp.ExecException | null, stdout: string, stderr: string) => {
						if (err) {
							reject(stderr);
						} else {
							resolve("success");
						}
					};
					cp.exec(sh, kd);

				}).then(() => {
					vscode.window.showInformationMessage("Upload file to server success.");
				}, (stderr) => {
					vscode.window.showErrorMessage("Error:", String(stderr));

				});
				progress.report({ increment: 100 });

			});


		}

	});
	context.subscriptions.push(disposable);
	let statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 50);
	statusBarItem.command = 'ether-remote.uploadFile';
	statusBarItem.tooltip = "Upload file";
	statusBarItem.text = "$(cloud-upload) Upload to Server";
	statusBarItem.show();
	context.subscriptions.push(statusBarItem);
}

// this method is called when your extension is deactivated
export function deactivate() { }
